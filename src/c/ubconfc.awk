BEGIN {
	FS=";"
	catidx=0
	rulidx=0
	blockidx=0
	cblckidx=0
	sblckidx=0
	blockb=-1
	blockl=0
	digs="0123456789ABCDEF"
	for(i=0;i<16;i++)
	{
		hex[substr(digs,i+1,1)]=i;
	}
}
function em1(a)
{
	if(a=="") return "-1"
	return "0x"a
}
function h2d(a)
{
	l=length(a)
	acc=0
	for(i=1;i<=l;i++)
	{
		acc=acc*16+hex[substr(a,i,1)];
	}
	return acc
}
function dumpblock()
{
	blkd=blockb ", " blockl ", &rule" rules[blockr]
	blocks[blockidx]=blkd
	blockidx++
	if(blockb<=256) lat1idx++
	split(blockr,rsp,",")
	if(substr(rsp[3],2,1)=="1")
	{
		cblcks[cblckidx]=blkd
		cblckidx++
	}
	if(rsp[1]=="GENCAT_ZS")
	{
		sblcks[sblckidx]=blkd
		sblckidx++
	}
	blockb=self
	blockl=1
	blockr=rule
}
{
	name=$2
	cat=toupper($3)
	self=h2d($1)
	up=h2d($13)
	low=h2d($14)
	title=h2d($15)
	convpos=1
	if((up==0)&&(low==0)&&(title==0)) convpos=0
	if(up==0) up=self
	if(low==0) low=self
	if(title==0) title=self
	updist=up-self
	lowdist=low-self
	titledist=title-self
	rule="GENCAT_"cat", NUMCAT_"cat", "((convpos==1)?                   \
				("1, " updist ", " lowdist ", " titledist): \
				("0, 0, 0, 0"))
	if(cats[cat]=="")
	{
		cats[cat]=(2^catidx);
		catidx++;
	}
	if(rules[rule]=="")
	{
		rules[rule]=rulidx;
		rulidx++;
	}
	if(blockb==-1)
	{
		blockb=self
		blockl=1
		blockr=rule
	}
	else
	{
		if (index(name,"First>")!=0)
		{
			dumpblock()
		}
		else if (index(name,"Last>")!=0)
		{
			blockl+=(self-blockb)
		}
		else if((self==blockb+blockl)&&(rule==blockr)) blockl++
		else
		{
			dumpblock()
		}
	}
}
END {
	dumpblock()
	for(c in cats) print "#define GENCAT_"c" "cats[c]
	print "#define MAX_UNI_CHAR " self
	print "#define NUM_BLOCKS " blockidx
	print "#define NUM_CONVBLOCKS " cblckidx
	print "#define NUM_SPACEBLOCKS " sblckidx
	print "#define NUM_LAT1BLOCKS " lat1idx
        print "#define NUM_RULES " rulidx
	for(r in rules)
	{
		printf "static const struct _convrule_ rule" rules[r] "={" r "};\n"
	}
	print "static const struct _charblock_ allchars[]={"
	for(i=0;i<blockidx;i++)
	{
		printf "\t{" blocks[i] "}"
		print (i<(blockidx-1))?",":"" 
	}
	print "};"
	print "static const struct _charblock_ convchars[]={"
	for(i=0;i<cblckidx;i++)
	{
		printf "\t{" cblcks[i] "}"
		print (i<(cblckidx-1))?",":""
	}
        print "};"
        print "static const struct _charblock_ spacechars[]={"
        for(i=0;i<sblckidx;i++)
        {       
                printf "\t{" sblcks[i] "}"
                print (i<(sblckidx-1))?",":""
        }       
	print "};"
}