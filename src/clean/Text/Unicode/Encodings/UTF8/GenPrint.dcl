definition module Text.Unicode.Encodings.UTF8.GenPrint

from Text.Unicode.Encodings.UTF8 import :: UTF8

from Text.GenPrint import generic gPrint, class PrintOutput, :: PrintState

derive gPrint UTF8
