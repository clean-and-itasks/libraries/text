implementation module Text.Unicode.Encodings.UTF8

import Data.Func
import Data.GenHash
import Data.GenLexOrd
import Data.List
import Data.Word8
import StdEnv

from Text.Unicode.UChar import instance fromInt UChar, instance toInt UChar
import Text.Unicode

:: UTF8 =: UTF8 String

instance fromUnicode UTF8
where
	fromUnicode :: !UString -> UTF8
	fromUnicode ustr = UTF8 (encodeString ustr)

instance toUnicode UTF8
where
	toUnicode :: !UTF8 -> UString
	toUnicode (UTF8 str) = decodeString str

empty :: UTF8
empty = UTF8 ""

utf8StringCorrespondingTo :: !String -> ?UTF8
utf8StringCorrespondingTo str = if (isValidUtf8 str) (?Just $ UTF8 str) ?None

instance toString UTF8
where
	toString :: !UTF8 -> {#Char}
	toString (UTF8 str) = str

instance == UTF8
where
 (==) :: !UTF8 !UTF8 -> Bool
 (==) a b
	= code inline {
		.d 2 0
			jsr eqAC
		.o 0 1 b
	}

instance < UTF8
where
 (<) :: !UTF8 !UTF8 -> Bool
 (<) a b
	= code inline {
		.d 2 0
			jsr cmpAC
		.o 0 1 i
			pushI 0
			gtI
	}

/**
 * This is based on Table 3-7. Well-Formed UTF-8 Byte Sequences
 * https://www.unicode.org/versions/Unicode15.0.0/UnicodeStandard-15.0.pdf
 * -----------------------------------------------------------------------------
 * |  Code Points        | First Byte | Second Byte | Third Byte | Fourth Byte |
 * |  U+0000..U+007F     |     00..7F |             |            |             |
 * |  U+0080..U+07FF     |     C2..DF |      80..BF |            |             |
 * |  U+0800..U+0FFF     |         E0 |      A0..BF |     80..BF |             |
 * |  U+1000..U+CFFF     |     E1..EC |      80..BF |     80..BF |             |
 * |  U+D000..U+D7FF     |         ED |      80..9F |     80..BF |             |
 * |  U+E000..U+FFFF     |     EE..EF |      80..BF |     80..BF |             |
 * |  U+10000..U+3FFFF   |         F0 |      90..BF |     80..BF |      80..BF |
 * |  U+40000..U+FFFFF   |     F1..F3 |      80..BF |     80..BF |      80..BF |
 * |  U+100000..U+10FFFF |         F4 |      80..8F |     80..BF |      80..BF |
 * -----------------------------------------------------------------------------
 */
isValidUtf8 :: !String -> Bool
isValidUtf8 str = isValidUtf8` 0
where
	isValidUtf8` :: !Int -> Bool
	isValidUtf8` i
		| bytesLeft >= 1 && curByte <= '\x7f' = isValidUtf8` $ i + 1
		| bytesLeft >= 2 && isInRange '\xc2' '\xdf' curByte && isInRange '\x80' '\xbf' curByte2 = isValidUtf8` $ i + 2
		| bytesLeft >= 3 && curByte == '\xe0' && isInRange '\xa0' '\xbf' curByte2  && isInRange '\x80' '\xbf' curByte3 =
			isValidUtf8` $ i + 3
		|	bytesLeft >= 3 &&
			isInRange '\xe1' '\xec' curByte && isInRange '\x80' '\xbf' curByte2  && isInRange '\x80' '\xbf' curByte3 =
			isValidUtf8` $ i + 3
		|	bytesLeft >= 3 &&
			curByte == '\xed' && isInRange '\x80' '\x9f' curByte2  && isInRange '\x80' '\xbf' curByte3 =
			isValidUtf8` $ i + 3
		|	bytesLeft >= 3 &&
			isInRange '\xee' '\xef' curByte && isInRange '\x80' '\xbf' curByte2  && isInRange '\x80' '\xbf' curByte3 =
			isValidUtf8` $ i + 3
		|	bytesLeft >= 4 &&
			curByte == '\xf0' && isInRange '\x90' '\xbf' curByte2 &&
			isInRange '\x80' '\xbf' curByte3 && isInRange '\x80' '\xbf' curByte4 =
			isValidUtf8` $ i + 4
		|	bytesLeft >= 4 &&
			isInRange '\xf1' '\xf3' curByte && isInRange '\x80' '\xbf' curByte2 &&
			isInRange '\x80' '\xbf' curByte3 && isInRange '\x80' '\xbf' curByte4 =
			isValidUtf8` $ i + 4
		|	bytesLeft >= 4 &&
			curByte == '\xf4' && isInRange '\x80' '\x8f' curByte2 &&
			isInRange '\x80' '\xbf' curByte3 && isInRange '\x80' '\xbf' curByte4 =
			isValidUtf8` $ i + 4
		| otherwise = bytesLeft == 0 // no valid range found, so the string is valid if no bytes are left
	where
		bytesLeft = length - i
		curByte  = str.[i]
		curByte2 = str.[i + 1]
		curByte3 = str.[i + 2]
		curByte4 = str.[i + 3]

	length = size str

	isInRange :: !Char !Char !Char -> Bool
	isInRange lower upper byte = lower <= byte && byte <= upper

// | Encode a string using 'encode' and store the result in a 'String'.
encodeString :: UString -> String
encodeString xs = bytesToString (encode xs)

// | Decode a string using 'decode' using a 'String' as input.
// | This is not safe but it is necessary if UTF-8 encoded text
// | has been loaded into a 'String' prior to being decoded.
decodeString :: String -> UString
decodeString xs = decode (stringToBytes xs)

replacement_character :: Int
replacement_character = 0xfffd

// | Encode a Haskell String to a list of Word8 values, in UTF8 format.
encode :: UString -> [Word8]
encode us = map fromInt (encode` (map toInt us))

encode` us = concatMap go us
	where
		go oc	| oc <= 0x7f    = [oc]
			 	| oc <= 0x7ff	= [ 0xc0 + (oc >> 6)
                		          , 0x80 + (oc bitand 0x3f)
                        		  ]

				| oc <= 0xffff  = [ 0xe0 + (oc >> 12)
                        		  , 0x80 + ((oc >> 6) bitand 0x3f)
                        		  , 0x80 + (oc bitand 0x3f)
                        		  ]

   			       				= [ 0xf0 + (oc >> 18)
		                          , 0x80 + ((oc >> 12) bitand 0x3f)
              			          , 0x80 + ((oc >> 6) bitand 0x3f)
                        		  , 0x80 + (oc bitand 0x3f)
                        		  ]

// | Decode a UTF8 string packed into a list of Word8 values, directly to String
decode :: [Word8] -> UString
decode ws = map fromInt (decode` (map toInt ws))

decode` [    ] = []
decode` [c:cs]
  | c < 0x80  = [c : decode` cs]
  | c < 0xc0  = [replacement_character : decode` cs]
  | c < 0xe0  = multi1 cs
  | c < 0xf0  = multi_byte 2 0xf  0x800
  | c < 0xf8  = multi_byte 3 0x7  0x10000
  | c < 0xfc  = multi_byte 4 0x3  0x200000
  | c < 0xfe  = multi_byte 5 0x1  0x4000000
  | otherwise = [replacement_character : decode` cs]
  where
    multi1 [c1 : ds] | (c1 bitand 0xc0) == 0x80 =
        let d = ((c bitand 0x1f) << 6) bitor (c1 bitand 0x3f)
        in if (d >= 0x000080) [d : decode` ds]
                              [replacement_character : decode` ds]
    multi1 _ = [replacement_character : decode` cs]

    multi_byte :: Int Int Int -> [Int]
    multi_byte i mask overlong = aux i cs (c bitand mask)
      where
        aux 0 rs acc
          | (overlong <= acc) && (acc <= 0x10ffff) &&
            (acc < 0xd800 || 0xdfff < acc) &&
            (acc < 0xfffe || 0xffff < acc)
        			= [acc : decode` rs]
					= [replacement_character : decode` rs]

        aux n [r:rs] acc | (r bitand 0xc0) == 0x80
        		= aux (n-1) rs ((acc << 6) bitor (r bitand 0x3f))
        aux _ rs _
        		= [replacement_character : decode` rs]

derive gHash UTF8
derive gLexOrd UTF8
