definition module Text.Unicode.Encodings.UTF8

/**
 * Ported to Clean from GHC by László Domoszlai, 2013-09-27
 * http://hackage.haskell.org/package/utf8-string-0.3.6/docs/src/Codec-Binary-UTF8-String.html
 *
 * Module      :  Codec.Binary.UTF8.String
 * Copyright   :  (c) Eric Mertens 2007
 * License     :  3-clause BSD license (see the LICENSE.BSD3 file)
 *
 * Maintainer:    emertens@galois.com
 * Stability   :  experimental
 * Portability :  portable
 *
 * Support for encoding UTF8 Strings to and from @[Word8]@
 *
 * `utf8StringCorrespondingTo`/`isValidUtf8`: (c) TOP Software Technology 2022
 *
 * @property-bootstrap
 *     import Data.Func, Data.Maybe
 *     import Gast.Gen
 *     import StdEnv
 *     import Text.GenPrint
 *
 *     import Text.Unicode
 *     import Text.Unicode.UChar.Gast
 *     import Text.Unicode.UChar.GenPrint
 *     import Text.Unicode.Encodings.UTF8.Gast
 *     import Text.Unicode.Encodings.UTF8.GenPrint
 *
 *     utf8FromUnicode :: !UString -> UTF8
 *     utf8FromUnicode str = fromUnicode str
 */

from Data.GenHash import generic gHash
from Data.GenLexOrd import generic gLexOrd, :: LexOrd
from StdClass import class <
from StdInt import instance * Int, instance + Int, instance toInt Char
from StdOverloaded import class *, class +, class toInt
from StdString import instance toString Real

from Text.Unicode import class fromUnicode, class toUnicode

:: UTF8 (=: UTF8 String)

instance fromUnicode UTF8
instance toUnicode UTF8

instance toString UTF8

/**
 * @property is reflexive: A.a :: UTF8:
 *     a =.= a
 * @property is symmetric: A.a :: UTF8; b :: UTF8:
 *     a =.= b ==> b =.= a
 * @property is transitive: A.a :: UTF8; b :: UTF8; c :: UTF8:
 *     a =.= b /\ b =.= c ==> a =.= c
 */
instance == UTF8 :: !UTF8 !UTF8 -> Bool :== code { .d 2 0 ; jsr eqAC ; .o 0 1 b	}
instance < UTF8 :: !UTF8 !UTF8 -> Bool	:== code { .d 2 0 ; jsr cmpAC ; .o 0 1 i ; pushI 0 ; gtI }

//* The empty UTF8 string.
empty :: UTF8

/**
 * The UTF8 value corresponding to the provided string, given that the string is valid UTF8.
 *
 * @property UTF8 string from unicode is valid: A.unicodeStr :: UString:
 *     isJust $ utf8StringCorrespondingTo (toString $ utf8FromUnicode unicodeStr)
 */
utf8StringCorrespondingTo :: !String -> ?UTF8

//* A predicate indicating whether the string is valid UTF8.
isValidUtf8 :: !String -> Bool

derive gHash UTF8
derive gLexOrd UTF8
