definition module Text.Unicode.UChar.GenPrint

from Text.GenPrint import generic gPrint, :: PrintState, class PrintOutput
from Text.Unicode.UChar import :: UChar

derive gPrint UChar