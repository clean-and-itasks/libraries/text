implementation module Text.Unicode.UChar.GenPrint

import Text.GenPrint
import Text.Unicode.UChar

gPrint{|UChar|} uchar st = gPrint{|*|} (toChar uchar) st