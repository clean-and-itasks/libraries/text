# Changelog

#### 2.0.2

- Enhancement: derive gLexOrd for `:: UTF8`.

#### 2.0.1

- Chore: support base `3.0`.

## 2.0.0

- Removed: Text.Unicode.Encodings.UTF8.Gast module.
- Removed: Text.Unicode.UChar.Gast module.

### 1.1.0

- Feature: add `gHash` derivation for `:: UTF8`.

## 1.0.0

- Initial version, import modules from clean platform v0.3.34 and destill all
  text modules.
- Generate `WCsubst.c` from the latest unicode standard instead of using a static one.
