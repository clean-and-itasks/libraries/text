# text

This library provides text functionality such as operations on strings and
encodings.

## Licence

The package is licensed under the BSD-2-Clause license; for details, see the
[LICENSE](/LICENSE) file.

Some modules were ported from Haskell and they are provided under the
compatible Haskell's 3-clause BSD license (see [LICENSE.BSD3](LICENSE.BSD3)).

- Text.Unicode.Encodings.UTF8
